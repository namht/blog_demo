'use strict'
const express = require('express');
const router = express.Router();
const config   = require('./../config');
const controller  = require(config.controllers + '/posts.api');

router.use(function(req, res, next) {
    // Check token here
    console.log("=============== REQUEST =================");
    console.log(req.method);
    console.log(req.originalUrl);
    console.log(req.headers);
    console.log(req.params);
    console.log(req.body);
    console.log("=============== END REQUEST =================");
    next();
});

/** GET **/
router.get('/', controller.index); 
router.get('/search', controller.search);
router.get('/:id', controller.detail);

/** POST **/
router.post('/', controller.create); 

/** PUT **/
router.put('/:id', controller.update); 

/** DELETE **/
router.delete('/:id', controller.delete); 

/** EXPORT **/
module.exports = router;