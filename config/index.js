'use strict';

var path = require('path');
const root = path.normalize(__dirname + '/..');
//Merge file module
const _ = require('lodash');
const dotenv = require('dotenv');
dotenv.config();
// All configurations will extend these options
// ============================================
console.log("process.env.NODE_ENV", process.env.NODE_ENV);
var all = {
	env: process.env.NODE_ENV || "local",

	// Root path of server
	root: root,
	controllers: root + '//api_controller',
	models: root + '//models',
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
	all,
	require('./env/' + all.env + '.js') || {});
