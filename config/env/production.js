var moment = require('moment');
module.exports = {
    mysql:{
        host: '167.71.203.153',
        user: 'blog',
        password: 'interview',
        database: 'blog',
        timezone: 'UTC',
        typeCast: function (field, next) {
            if (field.type == 'DATETIME' || field.type == 'TIMESTAMP') {
                let date = moment(field.string()).format('YYYY-MM-DD HH:mm:ss');
                if(date == "Invalid date"){
                    return null;
                }
                return date;
            }
            return next();
        },
        connectionLimit:10,
    },
};