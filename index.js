let express = require('express');
let path = require('path')
let app = express();
// const Controller = require("./controller");
const port = process.env.PORT || 5000;
app.listen(port)
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type");
    res.setHeader("Access-Control-Allow-Credentials", true);
    next();
});

app.use('/client',express.static(path.join(__dirname, 'client')))
app.use('/assets',express.static(path.join(__dirname, 'assets')))
app.get('/',function(req,res){
    res.sendFile(__dirname + '/client/index.html');
})

require('./route')(app);
console.log(`Server running port ${port}`)