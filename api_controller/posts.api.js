'use strict'
const config   = require('./../config');
const helper = require(config.root+'/helper');
const post_model = require(config.models+'/post.model');
const comment_model = require(config.models+'/comment.model');
function Post() {
    this.index = async (req, res) => {
        try {           
            let query = req.query;
            let post = await post_model.listByOption(query);
            return helper.successHandler(res, post); 
        } catch (e) {
            console.log(e);
            return helper.errorHandler(res, e);
        }
    };

    this.detail = async (req, res) => {
        try{
            let id = req.params.id;
            if(!id) throw new Error('InvalidFormat');
            let post = await post_model.findById(id);
            if(!post) throw new Error('Post Invalid');
            let comments = await comment_model.listByOption({post_id: post.post_id});
            post.comments = comments;
            return helper.successHandler(res, post); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    this.create = async (req, res) => {
        try{
            let data  = req.body;
            let {user_id} = req.headers;
            if(!user_id) throw new Error('InvalidHeader');
            if(!data.title || !data.description || !data.content){
                throw new Error('InvalidFormat');
            }
            data.is_active = data.is_active ? 1: 0;
            data.user_id = user_id;
            let created_result = await post_model.create(data);
            return helper.successHandler(res, created_result); 
        }catch(ex){
             return helper.errorHandler(res, ex);
        }
    };

    this.update = async (req, res) => {
        try{
            let id = req.params.id;
            let {title, description, content, is_active} = req.body;
            if(!id) throw new Error('InvalidFormat');
            let post = await post_model.findById(id);

            if(title) post.title = title;
            if(description) post.description = description;
            if(content) post.content = content;
            if(is_active) post.is_active = is_active;

            let update_result = await post_model.updateOne(post, post.post_id);

            return helper.successHandler(res, update_result); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    this.delete = async (req, res) => {
         try{
            let id = req.params.id;
            if(!id) throw new Error('InvalidFormat');
            let post = await post_model.findById(id);
            if(!post) throw new Error("Post is not exists");

            let result = await post_model.deleteOne(post.post_id);
            return helper.successHandler(res, result); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    this.search = async (req, res) => {
        try {           
            let {text} = req.query;
            if(!text) text = '';
            let posts = await post_model.searchText(text);
            return helper.successHandler(res, posts); 
        } catch (e) {
            console.log(e);
            return helper.errorHandler(res, e);
        }
    };

    return this;
}

// Exports
module.exports = new Post();