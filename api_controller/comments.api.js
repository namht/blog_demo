'use strict'
const config   = require('./../config');
const helper = require(config.root+'/helper');
const comment_model = require(config.models+'/comment.model');
function Controller() {
    this.index = async (req, res) => {
       
    };

    this.detail = async (req, res) => {
       
    };

    this.create = async (req, res) => {
        try{
            let data  = req.body;
            if(!data.user_id || !data.post_id || !data.text){
                throw new Error('InvalidFormat');
            }

            let created = await comment_model.create(data);
            return helper.successHandler(res, created); 
        }catch(ex){
             return helper.errorHandler(res, ex);
        }
    };

    this.update = async (req, res) => {
        
    };

    this.delete = async (req, res) => {
        
    };

    return this;
}

// Exports
module.exports = new Controller();