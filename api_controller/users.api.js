'use strict'
const config   = require('./../config');
const helper = require(config.root+'/helper');
const user_model = require(config.models+'/user.model');
const md5 = require("md5");
function Banner() {
    this.index = async (req, res) => {
        try {           
            let banner = await user_model.find();
            return helper.successHandler(res, banner); 
        } catch (e) {
            console.log(e);
            return helper.errorHandler(res, e);
        }
    };

    this.detail = async (req, res) => {
        try{
            let id = req.params.id;
            if(!id) throw new Error('InvalidFormat');
            let user = await user_model.findById(id);
            return helper.successHandler(res, user); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, e);
        }
    };

    this.create = async (req, res) => {
        try{
            let data  = req.body;
            if(!data.name || !data.email || !data.password){
                throw new Error('InvalidFormat');
            }

            let exists  = await user_model.findOne({where: 'email = ?', bind: [data.email]});
            if(exists) throw new Error('Email is exists');
            data.is_active = 1;
            data.password = md5(data.password);
            let created_result = await user_model.create(data);
            return helper.successHandler(res, created_result); 
        }catch(ex){
             return helper.errorHandler(res, ex);
        }
    };

    this.update = async (req, res) => {
        try{
            let id = req.params.id;
            let {name, email} = req.body;
            if(!id) throw new Error('InvalidFormat');
            let user = await user_model.findById(id);
            if(!user) throw new Error('User not exists');

            if(name) user.name = name;
            if(email) user.email = email;

            let update_result = await user_model.updateOne(user, user.user_id);
            return helper.successHandler(res, update_result); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    this.delete = async (req, res) => {
         try{
            let id = req.params.id;
            if(!id) throw new Error('InvalidFormat');
            let user = await user_model.findById(id);
            if(!user) throw new Error("User is not exists");

            let result = await user_model.deleteOne(user.user_id);
            return helper.successHandler(res, result); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    this.login = async (req, res) => {
        try{
            let {email, password} = req.body;
            if(!email || !password) throw new Error('InvalidFormat');
            let user = await user_model.findOne({
                where: 'email = ? AND password = ?',
                bind: [email, md5(password)]
            });
            if(!user)  throw new Error('Email or password is not correct');
            return helper.successHandler(res, user); 
        }catch(ex){
            console.log(ex);
             return helper.errorHandler(res, ex);
        }
    };

    return this;
}

// Exports
module.exports = new Banner();