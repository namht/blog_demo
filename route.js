module.exports = function (app) {
    var bodyParser = require('body-parser');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));  

    /** Call route api file **/
    const post_route = require('./routers/posts.route');
    const user_route = require('./routers/users.route');
    const comment_route = require('./routers/comments.route');

    /** Route API parent **/
    app.use('/posts', post_route);
    app.use('/users', user_route);
    app.use('/comments', comment_route);
    

};