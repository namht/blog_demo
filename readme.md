#Project Title
Blog demo with nodejs and angularjs

#Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

#Prerequisites
What things you need to install the software and how to install them

#Installing
 1.Makesure install nodejs
 2.Clone source: git clone https://namht@bitbucket.org/namht/blog_demo.git
 3.cd blog_demo
 4.npm install
 5.npm start
 6.config .env file
 7.change config/env/local.js mysql configuration 
 8.Launch: http://localhost:5000 (OR your port config)

#deloy: 
 - Heroku link: https://desolate-everglades-34910.herokuapp.com
