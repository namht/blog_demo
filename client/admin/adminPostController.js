var POSTS_DETAIL_URL = '/posts/:id';
var POSTS_CREATE_URL = '/posts';
angular.module("myApp").controller('AdminPostController', function($scope, $http, $window, $routeParams) {
    $scope.id = $routeParams.id;
	$scope.post = {};
    $scope.user = {};
    $scope.init = function(){
        if($scope.id && $scope.id != 'add'){
            $scope.getPost($scope.id);
        }

        let user = JSON.parse(localStorage.getItem('user'));
        if(!user) return $window.alert('You must be login');
        $scope.user = user;
    }

    $scope.getPost = function (id){
    	$http({
            method: "GET",
            url: POSTS_DETAIL_URL.replace(':id', id)
        }).then(function (result) {
        	let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
            $scope.post = res.data;
        })
    }

    $scope.updatePost = function (){
        if(!$scope.user.user_id) return $window.alert('You must be login');
        if(!$scope.post.title || !$scope.post.description || !$scope.post.content){
            return $scope.error_msg = 'Please fill full infomation';
        }
        if($scope.post.post_id) return $scope.editPost();
        return $scope.createPost();
    }

    $scope.editPost =function(){
        $http({
            method: "PUT",
            headers:{
                'user_id': $scope.user.user_id
            },
            data: $scope.post,
            url: POSTS_DETAIL_URL.replace(':id', $scope.id)
        }).then(function (result) {
            let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
            $window.location.href = '/#!/admin';
        })
    }

    $scope.createPost = function (){
        $http({
            method: "POST",
            headers:{
                'user_id': $scope.user.user_id
            },
            data: $scope.post,
            url: POSTS_CREATE_URL
        }).then(function (result) {
            let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
             $window.location.href = '/#!/admin';
        })
    }
});