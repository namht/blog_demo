var POSTS_URL = '/posts';
var POSTS_DETAIL_URL = '/posts/:id';
angular.module("myApp").controller('AdminController', function($scope, $http, $window) {
	$scope.list = [];
    $scope.init = function(){
        $scope.getList();
    }
    $scope.getList = function (){
        let user = JSON.parse(localStorage.getItem('user'));
        if(!user) return $window.alert('You must be login');
    	$http({
            method: "GET",
            params: {
                user_id: user.user_id
            },
            url: POSTS_URL
        }).then(function (result) {
        	let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }
            $scope.list = res.data;
            console.log($scope.list);
        })
    }
    $scope.Delete = function (item){
        let user = JSON.parse(localStorage.getItem('user'));
        if(!user) return $window.alert('You must be login');
        if(!confirm("Do you want to delete this item!")) return false;
        $http({
            method: "DELETE",
            headers: {
                user_id: user.user_id
            },
            url: POSTS_DETAIL_URL.replace(":id", item.post_id)
        }).then(function (result) {
            let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
            $window.location.reload();
        })
    }
});