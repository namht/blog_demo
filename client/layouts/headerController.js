angular.module("myApp").controller('HeaderController', function($scope, $http, $window) {
    $scope.user = false;
    $scope.init = function (){
    	$scope.user = JSON.parse(localStorage.getItem('user'));
    }

    $scope.logout = function(){
    	localStorage.removeItem('user');
    	$scope.user = false;
    	$window.location.href = '/#!/login';
        $window.location.reload();
    }

    $scope.search = function(){
    	let text = $scope.text;
    	console.log("$scope.text", $scope.text);
    	if(!text || text == ''){
    		$scope.error_msg = 'Please input at least a text';
    		return false;
    	}

    	$window.location.href = '/#!/search' + `?search=${text}`;
    }
});