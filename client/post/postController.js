var POSTS_DETAIL_URL = '/posts/:id';
var COMMENT_URL = '/comments';
angular.module("myApp").controller('PostController', function($scope, $http, $window, $routeParams) {
    $scope.id = $routeParams.id;
	$scope.post = {};
    $scope.user = false;
    $scope.comment_text = "";
    $scope.init = function(){
    	$scope.getPost($scope.id);
        $scope.user = JSON.parse(localStorage.getItem('user'));
    }

    $scope.getPost = function (id){
    	$http({
            method: "GET",
            url: POSTS_DETAIL_URL.replace(':id', id)
        }).then(function (result) {
        	let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
            $scope.post = res.data;
        })
    }

    $scope.postComment = function (){
        let text = $scope.comment_text;
        if(!text || text == ''){
            $scope.error_msg = 'Text comment is required';
            $window.alert($scope.error_msg);
            return false;
        }

        $http({
            method: "POST",
            data:{
                user_id: $scope.user.user_id,
                post_id: $scope.id,
                text: text
            },
            url: COMMENT_URL
        }).then(function (result) {
            let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                $window.alert($scope.error_msg);
                return false;
            }
            $scope.comment_text = '';
            $scope.getPost($scope.id);
        })
    }
});