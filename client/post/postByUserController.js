var POSTS_URL = '/posts';
angular.module("myApp").controller('PostByUserController', function($scope, $http, $window, $routeParams) {
    $scope.user_id = $routeParams.id;
	$scope.list = [];
    $scope.sort_created_at = 'desc';
    $scope.init = function(){
        $scope.getList();
    }
    $scope.getList = function (){
        if(!$scope.user_id) return $window.alert('User not found');
        console.log("$scope.user_id", $scope.user_id);
        $http({
            method: "GET",
            params: {
                user_id: $scope.user_id,
                sort_created_at: $scope.sort_created_at
            },
            url: POSTS_URL
        }).then(function (result) {
            let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }
            $scope.list = res.data;
            console.log($scope.list);
        })
    }
});