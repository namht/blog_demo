var POSTS_URL = '/posts';
angular.module("myApp").controller('HomeController', function($scope, $http, $window) {
	$scope.list = [];
    $scope.sort_created_at = 'desc';
    $scope.init = function(){
        $scope.getList();
    }
    $scope.getList = function (){
    	$http({
            method: "GET",
            params: {
                sort_created_at: $scope.sort_created_at
            },
            url: POSTS_URL
        }).then(function (result) {
        	let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }
            $scope.list = res.data;
            console.log($scope.list);
        })
    }
});