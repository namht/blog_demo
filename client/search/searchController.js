var SEARCH_URL = '/posts/search';
angular.module("myApp").controller('SearchController', function($scope, $http, $window, $location) {
	$scope.list = [];
    $scope.text = $location.search().search; //Get parameter from URL
    $scope.init = function(){
        console.log($scope.text);
        if(!$scope.text || $scope.text == ''){
            return $window.alert('Please search by at least a text');
        }
       $scope.getList();
    }
    $scope.getList = function (){
        $scope.error_msg = false;
    	$http({
            method: "GET",
            params: {
                text: $scope.text
            },
            url: SEARCH_URL
        }).then(function (result) {
        	let res = result.data;
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }
            $scope.list = res.data;
            console.log($scope.list);
        })
    }
});