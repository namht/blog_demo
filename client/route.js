var app = angular.module("myApp", ['ngRoute']);
app.config(['$routeProvider',
 function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/client/home/home.html',
        controller: 'HomeController'
      }).
      when('/home', {
        templateUrl: '/client/home/home.html',
        controller: 'HomeController'
      }).
      when('/login', {
        templateUrl: '/client/login/login.html',
        controller: 'LoginController'
      }).
       when('/register', {
        templateUrl: '/client/login/register.html',
        controller: 'LoginController'
      }).
      when('/post/:id', {
        templateUrl: '/client/post/post.html',
        controller: 'PostController'
      }).
      when('/user', {
        templateUrl: '/client/user/user.html',
        controller: 'UserController'
      }).
      when('/search', {
        templateUrl: '/client/search/search.html',
        controller: 'SearchController'
      }).
      when('/posts-by-user/:id', {
        templateUrl: '/client/post/post_by_user.html',
        controller: 'PostByUserController'
      }).
      when('/admin/post/:id', {
        templateUrl: '/client/admin/edit_post.html',
        controller: 'AdminPostController'
      }).
      when('/admin', {
        templateUrl: '/client/admin/admin.html',
        controller: 'AdminController'
      }).
      when('/error', {
        templateUrl: '/client/error/error.html',
      }).
      otherwise({
        redirectTo: '/error'
      });
 }]);

app.directive("page", [
  function() {
    return {
      templateUrl: "/client/layouts/paging.html",
      controller: function($scope, commonService) {
        $scope.limit = 1;
      }
    };
  }
]);
