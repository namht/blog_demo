var LOGIN_URL = '/users/login';
var REGISTER_URL = '/users';
angular.module("myApp").controller('LoginController', function($scope, $http, $window) {
	$scope.user = {};
    $scope.register = {};
    $scope.login = function (){
    	let {email, password} = $scope.user;
    	if(!email || !password){
    		$scope.error_msg = "Email or password is not empty";
            return false;
    	}

    	$http({
            method: "POST",
            data: {
                email, password
            },
            url: LOGIN_URL
        }).then(function (result) {
        	let res = result.data;
        	console.log(result);
        	console.log(res);
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }

            localStorage.setItem("user", JSON.stringify(res.data));
            $window.location.href = '/#!/admin';
            $window.location.reload();
        })
    }

    $scope.registerFn = function (){
        let {email, name, password, re_password} = $scope.register;
        if(!email || !password || !password || !re_password){
            $scope.error_msg = "Please fill full information";
            return false;
        }

        if(password != re_password){
            $scope.error_msg = "Password does not match";
            return false;
        }

        $http({
            method: "POST",
            data: {
                email, password, name
            },
            url: REGISTER_URL
        }).then(function (result) {
            let res = result.data;
            console.log(result);
            console.log(res);
            if (!res || !res.result || res.result == "false") {
                $scope.error_msg = res.error.message;
                return false;
            }

            localStorage.setItem("user", JSON.stringify(res.data));
            $window.location.href = '/#!/admin';
            $window.location.reload();
        })
    }
});