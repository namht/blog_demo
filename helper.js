function successHandler(res, data) {
     return res.send({
        result: true,
        data: data,
        error: null,
        status: 200
    });
}

function errorHandler(res, error) {
  console.error(error);
  let opt = {
    result: false,
    data: {},
    error: {
      name: error.name,
      status: error.status ? error.status : 500,
      message: error.message,
      description: error.message
    },
    status: error.status ? error.status : 500
  };
  return res.send(opt);
}

module.exports.successHandler = successHandler;
module.exports.errorHandler = errorHandler;