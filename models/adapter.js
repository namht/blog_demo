const db_connection   = require('./init');
function ObjectModel(){
    this.query = function(sql, bind){
        console.log(sql, bind);
         return new Promise((resolve, reject) => {
            db_connection.query(sql, bind, function(err, result){
                if (err) {
                    reject(new Error('ERROR_SQL_QUERY'));
                    return;
                }
                resolve(result);
            });
        });
    };

    this.findOne=function(table, conditions){
        let columns  = conditions['columns'] ? conditions['columns'] : "*";
        let sql = "SELECT "+columns+" FROM `"+table+"` WHERE " + conditions['where'];
        return new Promise((resolve, reject) => {
            db_connection.query(sql, conditions['bind'], function(err, result){
                if (err) {
                    reject(new Error('ERROR_SQL_QUERY'));
                    return;
                }
                if(result.length > 0){
                    resolve( result[0]);
                } else {
                    resolve( false);
                }
            });
        });
    };

    this.findById=function(table, primary_key, id){
        let sql = "SELECT * FROM `"+table+"` WHERE `"+primary_key+"` = ?";
        return new Promise((resolve, reject) => {
            db_connection.query(sql, [id], function(err, result){
                if (err) {
                    reject(new Error('ERROR_SQL_QUERY'));
                    return;
                }
                if(result.length > 0){
                    resolve( result[0]);
                } else {
                    resolve( false);
                }
            });
        });
    };

    this.find=function(table, conditions){
        let columns  = "*";
        let where    = '1=1';
        let bind     = [];
        let order    = '';
        let limit    = '';

        if(conditions){
            columns = conditions['columns'] ? conditions['columns'] : columns;
            where   = conditions['where'] ? conditions['where'] : where;
            bind    = conditions['bind'] ? conditions['bind'] : bind;
            order   = conditions['order'] ? ' ORDER BY ' + conditions['order'] : order;
            limit   = conditions['limit'] ? ' LIMIT ' + conditions['offset'] + ',' + conditions['limit'] : limit;
        }
        let sql = "SELECT "+ columns 
                +" FROM `"+table
                +"` WHERE " + where
                + order
                + limit;

        return new Promise((resolve, reject) => {
            db_connection.query(sql, bind, function(err, result){
                if (err) {
                    reject(new Error('ERROR_SQL_QUERY'));
                    return;
                }
                resolve(result);
            });
        });
    };

    this.count=function(table, conditions){
        let where    = '1=1';
        let bind     = [];

        if(conditions){
            where   = conditions['where'] ? conditions['where'] : where;
            bind    = conditions['bind'] ? conditions['bind'] : bind;
        }
        let sql = "SELECT count(*) as count" 
                +" FROM "+table
                +" WHERE " + where;

        return new Promise((resolve, reject) => {
            db_connection.query(sql, [bind], function(err, result){
                if (err) {
                    reject(new Error('ERROR_SQL_QUERY'));
                    return;
                }
                resolve(result[0].count);
            });
        });
    };

    this.create=function(table, queryParams){
        let sql = "INSERT INTO `"+table+"` SET ?";
        let bind = [queryParams];

        return new Promise((resolve, reject) => {
            db_connection.query(sql, bind, function(err, result){
                if (err) {
                    console.log(err);
                    //callback(error_message.ERROR_SQL_QUERY.msg);
                    reject(new Error('ERROR_SQL_QUERY'));
                    return false;
                }

                resolve(result);
                return true;
            });
        });
    };

    this.update=function(table, queryParams, conditions){
        let sql = "UPDATE `"+table+"` SET ? WHERE " + conditions['where'];

        let bind = [queryParams];

        for (var i = 0; i < conditions['bind'].length; i++) {
          bind.push(conditions['bind'][i]);
        } 

        return new Promise((resolve, reject) => {
            db_connection.query(sql, bind, function(err, result){
                if (err) {
                    console.log(err);
                    reject("model > update - Lỗi Truy Vấn");
                    return false;
                }

                resolve( result);
                return true;
            });
        });
    };

    this.delete=function(table, conditions){
        let sql = "DELETE FROM `"+table+"` WHERE " + conditions['where'];


        return new Promise((resolve, reject) => {
            db_connection.query(sql, conditions['bind'], function(err, result){
                if (err) {
                    console.log(err);
                    reject("model > delete - Lỗi Truy Vấn");
                    return false;
                }

                resolve( result);
                return true;
            });
        });
    };



    return this;
};

module.exports=new ObjectModel();
