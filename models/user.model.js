const adapter = require('./adapter');
const table = 'users';
const primary_key = 'user_id';
function PostModel(){
    this.findOne = function (conditions = {}){
        return adapter.findOne(table, conditions);
    }

    this.findById = function (id){
        return adapter.findById(table, primary_key, id);
    }

    this.find = function (conditions = {}){
        return adapter.find(table, conditions);
    }

    this.count = function (conditions = {}){
        return adapter.count(table, conditions);
    }

    this.create = async function (queryParams){
        try{
            let created = await adapter.create(table, queryParams);
            return  this.findById(created.insertId);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.updateOne = async function (queryParams, id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            let updated = await adapter.update(table, queryParams, conditions);
            return  this.findById(id);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.update = async function (queryParams, conditions = {}){
        return adapter.update(table, queryParams, conditions);
    }

    this.deleteOne = async function (id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            return await adapter.delete(table, conditions);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.delete = function (conditions = {}){
        return adapter.delete(table, conditions);
    }

    return this;
}

module.exports = new PostModel();
