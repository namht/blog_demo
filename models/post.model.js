const adapter = require('./adapter');
const table = 'posts';
const primary_key = 'post_id';
function PostModel(){
    this.findOne = function (conditions = {}){
        return adapter.findOne(table, conditions);
    }

    this.findById = function (id){
        return adapter.findById(table, primary_key, id);
    }

    this.find = function (conditions = {}){
        return adapter.find(table, conditions);
    }

    this.listByOption = function(options = {}){
        try{
            let query="SELECT"
                    +" p.*, u.name, u.email"
                    +" FROM posts p"
                    +" JOIN users u ON u.user_id = p.user_id"
                    +" WHERE 1=1";
            let bind = [];
            if(options.user_id){
                query += ' AND u.user_id = ?';
                bind.push(options.user_id);
            }

            if(options.sort_created_at){
                query +=" ORDER BY p.created_at " + options.sort_created_at
            }
            return adapter.query(query, bind);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.searchText = function(text){
        try{
            let query="SELECT"
                    +" p.*, u.name, u.email"
                    +" FROM posts p"
                    +" JOIN users u ON u.user_id = p.user_id"
                    +" WHERE p.title like ?"
                    +" LIMIT 20";
            let bind = ['%' + text+ '%'];
            return adapter.query(query, bind);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.count = function (conditions = {}){
        return adapter.count(table, conditions);
    }

    this.create = async function (queryParams){
        try{
            let created = await adapter.create(table, queryParams);
            return  this.findById(created.insertId);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.updateOne = async function (queryParams, id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            let updated = await adapter.update(table, queryParams, conditions);
            return  this.findById(id);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.update = async function (queryParams, conditions = {}){
        return adapter.update(table, queryParams, conditions);
    }

    this.deleteOne = async function (id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            return await adapter.delete(table, conditions);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.delete = function (conditions = {}){
        return adapter.delete(table, conditions);
    }

    return this;
}

module.exports = new PostModel();
