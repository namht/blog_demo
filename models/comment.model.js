const adapter = require('./adapter');
const table = 'comments';
const primary_key = 'comment_id';
function Model(){
    this.findOne = function (conditions = {}){
        return adapter.findOne(table, conditions);
    }

    this.findById = function (id){
        return adapter.findById(table, primary_key, id);
    }

    this.find = function (conditions = {}){
        return adapter.find(table, conditions);
    }

    this.listByOption = function(options = {}){
        try{
            let query="SELECT"
                    +" c.*, u.name, u.email"
                    +" FROM comments c"
                    +" JOIN users u ON u.user_id = c.user_id"
                    +" JOIN posts p ON p.post_id = c.post_id"
                    +" WHERE 1=1";
            let bind = [];
            if(options.user_id){
                query += ' AND u.user_id = ?';
                bind.push(options.user_id);
            }

            if(options.post_id){
                query += ' AND c.post_id = ?';
                bind.push(options.post_id);
            }

            if(options.sort_created_at){
                query +=" ORDER BY c.created_at " + options.sort_created_at
            }
            return adapter.query(query, bind);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.count = function (conditions = {}){
        return adapter.count(table, conditions);
    }

    this.create = async function (queryParams){
        try{
            let created = await adapter.create(table, queryParams);
            return  this.findById(created.insertId);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.updateOne = async function (queryParams, id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            let updated = await adapter.update(table, queryParams, conditions);
            return  this.findById(id);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.update = async function (queryParams, conditions = {}){
        return adapter.update(table, queryParams, conditions);
    }

    this.deleteOne = async function (id){
        try{
            let conditions = {
                where: "`"+primary_key+"` =?", bind: [id]
            };
            return await adapter.delete(table, conditions);
        }catch(ex){
            throw new Error(ex);
        }
    }

    this.delete = function (conditions = {}){
        return adapter.delete(table, conditions);
    }

    return this;
}

module.exports = new Model();
